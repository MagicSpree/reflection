package com.company.lababoutclasses;

import com.company.lababoutclasses.enums.VehicleColor;

public class VehicleDemo {

    public static void main(String[] args) {
        Truck truck = new Truck(2020, VehicleColor.LIGHT_GREEN, true);
        Car car = new Car(2015, VehicleColor.LIGHT_GREEN, true, false);
        printInfoAboutVehicle(truck);
        printInfoAboutVehicle(car);
    }

    private static void printInfoAboutVehicle(Vehicle vehicle) {
        System.out.println(vehicle.getVehicleInfo());
    }
}
