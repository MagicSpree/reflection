package com.company.lababoutclasses.enums;

import java.util.Arrays;

/**
 * Перечисление описывает возможные цвета автомобилей.
 */
public enum VehicleColor {
    RED("red"),
    BLUE("blue"),
    GREEN("green"),
    BLACK("black"),
    WHITE("white"),
    LIGHT_GREEN("light_green"),
    UNDEFINED("undefined");

    private String color;

    public String getColor() {
        return color;
    }

    VehicleColor(String color) {
        this.color = color;
    }

    /**
     * Метод возвращает объект {@link VehicleColor} на основе переданного названия цвета.
     *
     * @param color {@link String} название цвета
     * @return {@link VehicleColor}
     */
    public static VehicleColor getColorByName(String color) {
        return Arrays.stream(VehicleColor.values())
                .filter(vehicleColor -> vehicleColor.getColor().equals(color))
                .findFirst().orElse(VehicleColor.UNDEFINED);
    }
}
