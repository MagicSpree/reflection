package com.company;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class Reflection<T> {
    private final Class<T> nameClass;

    public Reflection(Class<T> nameClass) {
        this.nameClass = nameClass;
    }

    public void getAllInfoAboutClass() {
        printNameClass();
        printFieldsInfo();
        printParentName();
        printMethodInfo();
        printConstructorInfo();
    }

    /**
     * Вывод имени класса
     */
    private void printNameClass() {
        print("Имя класса: ");
        String fullNameClass = " Полное имя класса: " + nameClass.getName();
        String shortNameClass = " Краткое имя класса: " + nameClass.getSimpleName();
        String canonicalNameClass = " Каноничное имя класса: " + nameClass.getCanonicalName();
        print(fullNameClass);
        print(shortNameClass);
        print(canonicalNameClass);
    }

    /**
     * Вывод полей класса и их модификаторов;
     */
    private void printFieldsInfo() {
        print("Поля класса: ");
        Field[] fields = nameClass.getDeclaredFields();
        for (Field field : fields) {
            String fieldName = String.format(" Модификатор доступа: %s, Тип: %s, Наименование поля: %s",
                    Modifier.toString(field.getModifiers()),
                    field.getType().getSimpleName(),
                    field.getName()
            );
            print(fieldName);
        }
    }

    /**
     * Вывод имени родительского класса
     */
    private void printParentName() {
        print("Родительский класс: ");
        String fullNameClass = " Имя родительского класса: " + nameClass.getSuperclass().getSimpleName();
        print(fullNameClass);
    }

    /**
     * Вывод методов класса и их модификаторов
     */
    private void printMethodInfo() {
        print("Методы класса и их модификаторы: ");
        Method[] methods = nameClass.getDeclaredMethods();
        for (Method method : methods) {
            String methodName = String.format(" Модификатор доступа: %s, Тип: %s, Наименование метода: %s",
                    Modifier.toString(method.getModifiers()),
                    method.getAnnotatedReturnType().getType().getTypeName(),
                    method.getName()
            );
            print(methodName);
        }
    }

    /**
     * Вывод конструкторов класса
     */
    private void printConstructorInfo() {
        print("Конструкторы класса и их идентификаторы: ");
        Constructor<?>[] constructors = nameClass.getDeclaredConstructors();
        for (Constructor<?> constructor : constructors) {
            String methodName = String.format(" Входные параметры: %s, Наименование конструктора: %s",
                    Arrays.toString(constructor.getParameterTypes()),
                    constructor.getName()
            );
            print(methodName);
        }
    }

    private void print(String message) {
        System.out.println(message);
    }
}
