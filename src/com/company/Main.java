package com.company;

import com.company.lababoutclasses.Car;
import com.company.lababoutclasses.Vehicle;

public class Main {

    public static void main(String[] args) {
        Reflection<Vehicle> reflectionVehicle = new Reflection<>(Vehicle.class);
        Reflection<Car> reflectionCar = new Reflection<>(Car.class);

        getInfoAboutClass(reflectionVehicle);
        getInfoAboutClass(reflectionCar);
    }

    public static void getInfoAboutClass(Reflection<?> reflection) {
        reflection.getAllInfoAboutClass();
    }
}
